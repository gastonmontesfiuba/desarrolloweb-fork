var variableBooleana = false

if (!variableBooleana) {
	// este código se ejecuta?
}


let fila0 = [0, 1, 2, 3, 4, 5, 6, 7]
let fila1 = [3, 4, 5, 2, 4, 1]
let matriz = [fila0, fila1]
matriz[2][4]


let variableAutor = 'Franz Kafka'
let variableTítulo = 'La Metamorfosis'
let variableResumen = 'Resumen del libro'
let variableCapitulo1 = 'Capítulo 1 - Franz Kafka se convierte en un bicho'
let variableLongitud = 3005

let laMetamorfosis = new Map();
laMetamorfosis.set('estoEsUnaClave', variableAutor);
laMetamorfosis.set('titulo', variableTítulo);
laMetamorfosis.set('resumen', variableResumen);
laMetamorfosis.set('capitulo1', variableCapitulo1);
laMetamorfosis.set('longitud', variableLongitud);
laMetamorfosis.get('estoEsUnaClave') // -> Franz Kafka.
laMetamorfosis.get('capitulo2') // -> Undefined.


let mySet = new Set();
mySet.add(1);
mySet.add('algún texto');
mySet.add('foo');
mySet.add('foo2');
mySet.delete('foo3');
mySet.add(45)
mySet.size; // -> ?

for (let item of mySet) 
	console.log(item); // -> 1, algún texto.

// Algoritmo: Pedir al usuario 2 números y sumarlos.
1 - Pedir el primer número.
2 - Pedir el segundo número.
3 - Sumar los numeros.
4 - Mostrar resultado

// Undefined.
var variableNoDefinida;
typeof(variableNoDefinida) // -> undefined.

// Boolean.
var variableBoolean = 15 % 2 == 0
if (variableBoolean) {
	// No pasa por aca.
} else {
	// Pasa por aca.
}

// Null.
var variableNumber = 5
variableNumber = null
