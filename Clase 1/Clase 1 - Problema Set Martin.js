// Si agrego un objecto persona a un set con los mismos valores, el set los reconoce como iguales?
// Probarlo en https://jsfiddle.net/.
let mySet = new Set()
mySet.add("Hola")
mySet.add("Hola")

let myArray = new Array()
myArray.push("Hola")
myArray.push("Hola")

console.log(mySet.size) // -> 1.
console.log(myArray.length) // -> 2.

let personSet = new Set()
let person1 = { name: 'Gaston', age: 30 }
let person2 = { name: 'Gaston', age: 30 }

console.log(person1 == person2) // -> false

personSet.add(person1)
personSet.add(person2)

let personArray = new Array()
personArray.push(person1)
personArray.push(person2)

console.log(personSet.size) // -> 2.
console.log(personArray.length) // -> 2.

personSet.forEach(element => console.log(element.name)); // -> "Gaston", "Gaston"

let otherSet = new Set()
otherSet.add(person1)
otherSet.add(person1)

let otherArray = new Array()
otherArray.push(person1)
otherArray.push(person1)

console.log(otherSet.size) // -> 1.
console.log(otherArray.length) // -> 2.